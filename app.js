
// importing the module from the express

const mongoose = require('mongoose');
const express = require('express');
const path = require('path');
const app = express('');
// const dotenv = require('dotenv');
// dotenv.config({path: './config.env'});
const userRouter = require("./routes/userRoutes");
const viewRouter = require('./routes/viewRoutes')
const cookieParser = require('cookie-parser')


//middleware
app.use(cookieParser())
app.use(express.json()); //allow the express to process json
// app.use(express.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, "/views")))
app.use("/api/v1/users", userRouter);
app.use('/', viewRouter)

// app.get('/', (req, res) => {
//     res.status(200).sendFile('index.html');
// })

module.exports = app

